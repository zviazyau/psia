#pragma comment(lib, "ws2_32.lib")
#include "stdafx.h"
#include <winsock2.h>
#include "ws2tcpip.h"

#include <iostream>
#include <string>

#define BUFFERS_LEN 1024


#define MAX_FILENAME_SIZE 64

#define OPERATION_SEND 1
#define OPERATION_RECEIVE 2



bool is_number(const std::string& s)
{
	std::string::const_iterator it = s.begin();
	while (it != s.end() && std::isdigit(*it)) ++it;
	return !s.empty() && it == s.end();
}

bool is_ip_correct(std::string ip) {

	IN_ADDR tmp;
	if (inet_pton(AF_INET, ip.c_str(), &tmp) == 1) {
		return true;
	}
	std::cout << "IP is not correct!\n";
	return false;
}

bool get_int(int *in) {
	std::string input;
	std::getline(std::cin,input);
	if (is_number(input)) {
		*in = std::stoi(input);
		return true;
	}
	return false;
}

std::string get_target_ip() {
	std::string ip;
	do {
		std::cout << "Type receiver IP address (without port): ";
		std::getline(std::cin, ip);
	} while (!is_ip_correct(ip));
	return ip;
}
int get_port(std::string port_type) {
	int port = NULL;
	bool is_correct_port = false;
	do {
		std::printf("Type %s port: ", port_type.c_str());
		if (!get_int(&port)) {
			std::printf("Invalid port!\n");
		}
		is_correct_port = true;
	} while (!port);

	return port;
}

void InitWinsock()
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}

int main()
{
	int operation = NULL;
	do{
		std::cout << "Choose operation: \n Type '1' for send file.\n Type '2' to receive file.\n";
	} while (!get_int(&operation) || operation != OPERATION_SEND && operation != OPERATION_RECEIVE);

	int local_port = get_port("local");


	SOCKET socketS;

	InitWinsock();

	struct sockaddr_in local;
	struct sockaddr_in from;

	int fromlen = sizeof(from);
	local.sin_family = AF_INET;
	local.sin_port = htons(local_port); //set local port
	local.sin_addr.s_addr = INADDR_ANY;

	socketS = socket(AF_INET, SOCK_DGRAM, 0);
	if (bind(socketS, (sockaddr*)&local, sizeof(local)) != 0) {
		printf("Binding error!\n");
		return 1;
	}
	//**********************************************************************
	char buffer_rx[BUFFERS_LEN];
	char buffer_tx[BUFFERS_LEN];

	const std::string end_of_transmission_signal = "STOP";


	
	if (operation == OPERATION_SEND) {
		//Get IP and port from stdin
		std::string target_ip = get_target_ip();
		int target_port = get_port("target");

		sockaddr_in addrDest;
		addrDest.sin_family = AF_INET;
		addrDest.sin_port = htons(target_port); //set target port
		inet_pton(AF_INET, target_ip.c_str(), &(addrDest.sin_addr)); //set target ip



		// Get filename from stdin.
		std::cout << "Enter filename: ";
		std::string fname;
		std::getline(std::cin, fname);

		// Open file.
		FILE* f;
		if ((f = fopen(fname.c_str(), "rb")) == NULL) {
			fprintf(stderr, "Error while opening file �%s�\n", fname);
			return 0;
		}

		// Send filename.
		
		fname = fname.substr(fname.find_last_of("/\\") + 1); //send just filename without path
		sendto(socketS, fname.c_str(), fname.size() + 1, 0, (sockaddr*)&addrDest, sizeof(addrDest));

		// Get file size.
		fseek(f, 0, SEEK_END);
		int file_size = ftell(f);
		fseek(f, 0, SEEK_SET);

		// Send file size.
		((int*)buffer_tx)[0] = file_size;
		sendto(socketS, buffer_tx, sizeof(int), 0, (sockaddr*)&addrDest, sizeof(addrDest));

		// Read and send file.
		int bytes_read;
		while ((bytes_read = fread(buffer_tx, 1, BUFFERS_LEN, f)) != 0) {
			sendto(socketS, buffer_tx, bytes_read, 0, (sockaddr*)&addrDest, sizeof(addrDest));
			printf("Sent %d bytes\n", bytes_read);
		}

		// Close file.
		if (fclose(f) == EOF)
		{
			fprintf(stderr, "Error while closing file �%s�\n", fname);
			return 0;
		}

		// Send end of transmission signal.	
		sendto(socketS, end_of_transmission_signal.c_str(), end_of_transmission_signal.size() + 1, 0, (sockaddr*)&addrDest, sizeof(addrDest));

		closesocket(socketS);
	}
	else if (operation == OPERATION_RECEIVE) {

		// Receive filename.
		printf("Waiting for datagram ...\n");
		if (recvfrom(socketS, buffer_rx, sizeof(buffer_rx), 0, (sockaddr*)&from, &fromlen) == SOCKET_ERROR) {
			printf("Socket error!\n");
			return 1;
		}
		char fname[BUFFERS_LEN];
		memcpy(fname, buffer_rx, BUFFERS_LEN);

		// Receive filesize.
		if (recvfrom(socketS, buffer_rx, sizeof(buffer_rx), 0, (sockaddr*)&from, &fromlen) == SOCKET_ERROR) {
			printf("Socket error!\n");
			return 1;
		}
		int filesize = *((int*)buffer_rx);
		printf("Expected filesize: %d\n", filesize);

		// Open file.
		FILE* f;
		if ((f = fopen(fname, "wb")) == NULL)
		{
			fprintf(stderr, "Error while opening file �%s�\n", fname);
		}

		// Receive and write file.
		int received_bytes_this_packet;
		int received_bytes_total = 0;
		while ((received_bytes_this_packet = recvfrom(socketS, buffer_rx, sizeof(buffer_rx), 0, (sockaddr*)&from, &fromlen)) != 0) {
			printf("Received %d bytes\n", received_bytes_this_packet);
			if (strcmp(buffer_rx, end_of_transmission_signal.c_str()) == 0) {
				printf("Sender ended the transmission\n");
				break;
			}

			received_bytes_total += received_bytes_this_packet;
			int bytes_written = fwrite(buffer_rx, sizeof(char), received_bytes_this_packet, f);
		}

		// Check filesize against received bytes.
		if (filesize != received_bytes_total) {
			printf("%d bytes missing\n", filesize - received_bytes_total);
		}

		// Close file.
		if (fclose(f) == EOF)
		{
			fprintf(stderr, "Error while closing file �%s�\n", fname);
			return 0;
		}

		if (filesize == received_bytes_total) {
			printf("You receive file %s!", fname);
		}

		closesocket(socketS);
	}
	//**********************************************************************

	getchar();
	return 0;
}
